#include "track.hpp"


Track::Track(const std::string& name, const unsigned& randomizer)
{  
    date = std::time(nullptr);
    this->name = name;
    duration.tm_min = randomizer % 6;
    duration.tm_sec = randomizer % 60;
}

Track::~Track()
{
}

const std::string Track::getName() const
{
    return name;
}

void Track::getDate() const
{   
    std::cout << ctime(&date);
}

void Track::getDuration()
{
    std::cout << std::put_time(&duration, "%M:%S") << std::endl;
}
