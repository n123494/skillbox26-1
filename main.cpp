#include "player.hpp"
#include <iostream>
#include <string>


std::string getTrackName(std::string& input)
{
    std::string trackName = "";
    size_t space = input.find(' ');
    if (space != std::string::npos)
    {
        trackName = input.substr(space + 1);
        input = input.substr(0, space);
    }    
    return trackName;
}

int main(int argc, char **argv)
{    
    Player player;
    std::string input;
    
    std::cout << "play  - start track\n";
    std::cout << "play trackname - choose and start track by name\n";
    std::cout << "pause - pause track\n";
    std::cout << "next  - next track\n";
    std::cout << "stop  - stop player\n";
    std::cout << "exit  - turn off player\n\n";
    
    while (true){
        std::cout << "Enter command: ";
        std::getline(std::cin, input);
        std::string trackName = getTrackName(input);
        if (input == "play")
        {
            if (trackName != "")
            {
                player.play(trackName);
            }
            else
            {
                player.play();
            }
        }
        else if (input == "pause")
        {
            player.pause();
        }
        else if (input == "next")
        {
            player.next();
        }
        else if (input == "stop")
        {
            player.stop();
        }
        else if (input == "exit")
        {
            break;
        }
        else
        {
            std::cout << "Invalid input.\n";
        }
    }
    
	return 0;
}
