#pragma once
#include "track.hpp"
#include <iostream>
#include <list>
#include <string>
#include <ctime>
#include <random>

class Player
{
    const unsigned MAX_TRACK_NUM = 10;
    
    std::list<Track> tracklist;
    std::list<Track>::iterator track;
    bool isPlaying = false;
    bool isStopped = true;
    
public:
    Player();
    ~Player();
    
    void play();
    
    void play(const std::string& name);
    
    void pause();
    
    void next();
    
    void stop();
};

