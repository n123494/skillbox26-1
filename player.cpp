#include "player.hpp"

Player::Player()
{
    std::mt19937 engine;
    engine.seed(std::time(nullptr));
    for (unsigned i = 1; i <= MAX_TRACK_NUM; ++i)
    {
        tracklist.emplace_back("Track" + std::to_string(i), engine());
    }
    
    track = tracklist.begin();
}

Player::~Player()
{
}

void Player::play()
{
    if (!isPlaying && isStopped)
    {
        std::cout << "NOW PLAYING:\n";
        std::cout << "Name: " << track->getName() << std::endl;
        std::cout << "Created: ";
        track->getDate();
        std::cout << "Duration: ";
        track->getDuration();
        std::cout << std::endl;
        isStopped = false;
        isPlaying = true;
    }    
    else if (!isPlaying && !isStopped)
    {
        std::cout << "Track playing continued.\n";
    }    
    else
    {
        std::cout << "Track is already playing\n";
    }
}

void Player::play(const std::string& name)
{
    if (isPlaying)
    {
        std::cout << "Stop before change track.\n";
        return;
    }
    
    for (auto it = tracklist.begin(); it != tracklist.end(); ++it)
    {
        if (it->getName() == name)
        {
            track = it;
            play();
            return;
        }
    }
    std::cout << "Track with these name not exist.\n";
}

void Player::pause()
{
    if (isPlaying)
    {
        std::cout << "Track paused.\n";
        isPlaying = false;
    }
    else
    {
        std::cout << "Track is not playing.\n";
    }
}

void Player::next()
{
    ++track;
    if (track == tracklist.end()){
        track = tracklist.begin();
    }
    isPlaying = false;
    isStopped = true;
    play();
}

void Player::stop()
{
    if (isStopped)
    {
        std::cout << "Track is already stopped.\n";
    }
    else
    {
        std::cout << "Track stopped.\n";
        isStopped = true;
        isPlaying = false;
    }
}