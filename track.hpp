#pragma once
#include <string>
#include <ctime>
#include <iostream>
#include <iomanip>

class Track
{
    std::string name = "";
    time_t date;
    std::tm duration;
    
public:    

    Track(const std::string& name, const unsigned& randomizer);    
    ~Track();
    
    const std::string getName() const;
    
    void getDuration();
    
    void getDate() const;
};

